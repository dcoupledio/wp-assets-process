<?php namespace Decoupled\Wordpress\Process\Assets;

use Decoupled\Core\Bundle\BundleProcess;
use Decoupled\Core\Bundle\BundleInterface;
use Decoupled\Core\Application\ApplicationContainer;

class AssetProcess extends BundleProcess{

    public function __construct( ApplicationContainer $app )
    {
        $this->setApp( $app );
    }

    public function getName()
    {
        return 'wp.asset.process';
    }

    public function process( BundleInterface $bundle )
    {
        $path = $bundle->getDir().DIRECTORY_SEPARATOR.'assets.php';

        $action = $this->getActionFromFile( $path );

        if(!$action) return;

        $action();
    }
}