<?php

require('../vendor/autoload.php');

require('./mock.php');

use phpunit\framework\TestCase;
use Decoupled\Wordpress\Extension\Assets\WPAssetsExtension;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Wordpress\Process\Assets\AssetProcess;
use Decoupled\Wordpress\Process\Assets\Test\AppBundle;
use Decoupled\Core\Extension\Bundle\BundleExtension;


class ProcessTest extends TestCase{

    public function testCanUseExtension()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses( new DependencyInjectionExtension() );

        $app->uses( new WPAssetsExtension() );

        $app->uses( new BundleExtension() );

        $app->uses(function( $bundleInitializer ){

            $bundleInitializer->uses( new AssetProcess($this) );
        });

        $bundles = $app->bundle()->add( new AppBundle() );

        $app->process()->init( $app['$bundle.collection'] );

        $this->assertContains( 'jquery', WP_Mock::$regScripts );

        //$this->assertContains( 'jquery', WP_Mock::$enqScripts );
    }

}