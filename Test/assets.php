<?php 

use Decoupled\Wordpress\Assets\AssetConfig;

return function( AssetConfig $wpAssets ){

    $wpAssets->add( 'jquery', 'path/to/jquery.js' );

};